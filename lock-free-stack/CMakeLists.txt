add_gtest(test_stack test.cpp)

target_link_libraries(test_stack libhazard_ptr)

add_benchmark(bench_stack run.cpp)

target_link_libraries(bench_stack libhazard_ptr)
